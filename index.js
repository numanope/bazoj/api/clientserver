let ApiClient;

if (typeof window !== 'undefined') {
	ApiClient = await import('./src/ApiClientBrowser.js').then(module => module.default);
}
else {
	ApiClient = await import('./src/ApiClientServer.js').then(module => module.default);
}

export default ApiClient;
