import { ApiClient as shared } from './apishared';

function ApiClient() {
	// console.log('BROWSER');
	if (typeof window !== 'undefined') {
		shared.call(this);
		this.isOnline = function() {
			return window.navigator.onLine;
		};
		this.fetch = window.fetch.bind(window);
	}
	else {
		throw new Error('Not supported');
	}
}

ApiClient.prototype = Object.create(shared.prototype);
ApiClient.prototype.constructor = ApiClient;

export default ApiClient;